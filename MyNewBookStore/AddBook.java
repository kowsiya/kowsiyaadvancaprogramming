import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class AddBook extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_6;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddBook frame = new AddBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection conn = null;

	public AddBook() throws SQLException {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/MyNewBookStore", "root", "root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setTitle("Add newbook");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 543, 528);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setBounds(26, 38, 56, 16);
		contentPane.add(lblCategory);

		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(26, 75, 56, 16);
		contentPane.add(lblTitle);

		JLabel lblId = new JLabel("Id:");
		lblId.setBounds(26, 106, 56, 16);
		contentPane.add(lblId);

		JLabel lblAuthor = new JLabel("Author:");
		lblAuthor.setBounds(26, 142, 56, 16);
		contentPane.add(lblAuthor);

		JLabel lblNumbersOfPages = new JLabel("Numbers of pages:");
		lblNumbersOfPages.setBounds(26, 178, 122, 16);
		contentPane.add(lblNumbersOfPages);

		JLabel lblNewLabel = new JLabel("Edition:");
		lblNewLabel.setBounds(26, 210, 56, 16);
		contentPane.add(lblNewLabel);

		JLabel lblPublished = new JLabel("Published:");
		lblPublished.setBounds(26, 244, 91, 16);
		contentPane.add(lblPublished);

		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(26, 279, 91, 16);
		contentPane.add(lblDescription);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Arts & Music", "Biographies", "Business", "Kids",
				"Computers & Tech", "Cooking", "Health & Fitness", "History", "Medical", "Science & Math", "Other" }));
		comboBox.setBounds(176, 35, 163, 22);
		contentPane.add(comboBox);

		textField = new JTextField();
		textField.setBounds(175, 72, 164, 22);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(176, 103, 163, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(175, 139, 165, 22);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(175, 175, 163, 22);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(175, 210, 163, 22);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(109, 244, 56, 16);
		contentPane.add(lblYear);

		JLabel lblMonth = new JLabel("Month");
		lblMonth.setBounds(240, 244, 56, 16);
		contentPane.add(lblMonth);

		JLabel lblNewLabel_1 = new JLabel("Date");
		lblNewLabel_1.setBounds(395, 244, 56, 16);
		contentPane.add(lblNewLabel_1);

		textField_6 = new JTextField();
		textField_6.setBounds(144, 241, 73, 22);
		contentPane.add(textField_6);
		textField_6.setColumns(10);

		JTextPane textPane = new JTextPane();
		textPane.setBounds(175, 291, 288, 91);
		contentPane.add(textPane);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] { "January", "February", "March", "April", "May",
				"June", "July", "August", "September", "October", "November", "December" }));
		comboBox_1.setBounds(286, 241, 96, 22);
		contentPane.add(comboBox_1);

		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(
				new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
						"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
		comboBox_2.setBounds(436, 241, 47, 22);
		contentPane.add(comboBox_2);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminOption.main(new String[] {});
			}
		});
		btnBack.setBounds(178, 425, 97, 25);
		contentPane.add(btnBack);

		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String category = comboBox.getSelectedItem().toString();
				String title = textField.getText();
				String id = textField_1.getText();
				String author = textField_2.getText();
				String pages = textField_3.getText();
				String edition = textField_4.getText();
				String year = textField_6.getText();
				String month = comboBox_1.getSelectedItem().toString();
				String date = comboBox_2.getSelectedItem().toString();
				String description = textPane.getText();

				try {
					String sql = ("insert into MyNewBookStore.add_book (Category,Title,Id,Author,NumbersOfPages,Edition,Year,Month,Date,Description) values('"
							+ category + "','" + title + "','" + id + "','" + author + "','" + pages + "','" + edition
							+ "','" + year + "','" + month + "','" + date + "','" + description + "')");
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Account created");
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnCreate.setBounds(315, 426, 97, 25);
		contentPane.add(btnCreate);

	}

}
