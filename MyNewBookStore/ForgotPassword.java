import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ForgotPassword extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ForgotPassword frame = new ForgotPassword();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection conn = null;

	public ForgotPassword() throws SQLException {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/MyNewBookStore", "root", "root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 479, 339);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblId = new JLabel("Id:");
		lblId.setBounds(24, 37, 56, 16);
		contentPane.add(lblId);

		JLabel lblUsername = new JLabel("User name:");
		lblUsername.setBounds(24, 66, 97, 16);
		contentPane.add(lblUsername);

		JLabel lblFullName = new JLabel("Full name:");
		lblFullName.setBounds(24, 96, 72, 16);
		contentPane.add(lblFullName);

		JLabel lblNewLabel = new JLabel("Your security question:");
		lblNewLabel.setBounds(24, 124, 140, 16);
		contentPane.add(lblNewLabel);

		JLabel lblAnswer = new JLabel("Answer:");
		lblAnswer.setBounds(24, 152, 56, 16);
		contentPane.add(lblAnswer);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(24, 181, 97, 16);
		contentPane.add(lblPassword);

		textField = new JTextField();
		textField.setBounds(170, 34, 140, 22);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(170, 63, 140, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(170, 93, 140, 22);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(170, 124, 140, 22);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(170, 152, 140, 22);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setBounds(170, 181, 140, 22);
		contentPane.add(textField_5);
		textField_5.setColumns(10);

		JButton btnNewButton = new JButton("Search");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String a1 = textField.getText();
				String sql = "select * from signUp where Id = '" + a1 + "'";

				try {
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);

					if (rs.next()) {
						textField_1.setText(rs.getString(2));
						textField_2.setText(rs.getString(3));
						textField_3.setText(rs.getString(8));
						rs.close();
						st.close();

					} else {
						JOptionPane.showMessageDialog(null, "incorrect");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, e2);
				}
			}
		});
		btnNewButton.setBounds(323, 33, 82, 25);
		contentPane.add(btnNewButton);

		JButton btnGetPassword = new JButton("Get Password");
		btnGetPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String a2 = textField_4.getText();
				String sql = "select * from signUp where Answer = '" + a2 + "'";

				try {
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					if (rs.next()) {
						textField_5.setText(rs.getString(4));
					} else {
						JOptionPane.showMessageDialog(null, "incorrect");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, e2);
				}

			}
		});
		btnGetPassword.setBounds(322, 148, 127, 25);
		contentPane.add(btnGetPassword);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login.main(new String[] {});
			}
		});
		btnBack.setBounds(12, 254, 97, 25);
		contentPane.add(btnBack);
	}

}
