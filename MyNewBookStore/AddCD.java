import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class AddCD extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddCD frame = new AddCD();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection conn = null;
	private JTextField textField;
	private JTextField textField_1;

	public AddCD() throws SQLException {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/MyNewBookStore", "root", "root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setTitle("Add newCD");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 552, 531);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAddNewcd = new JLabel("Add newCD");
		lblAddNewcd.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAddNewcd.setBounds(222, 13, 90, 16);
		contentPane.add(lblAddNewcd);

		JLabel lblLanguage = new JLabel("Language:");
		lblLanguage.setBounds(24, 51, 90, 16);
		contentPane.add(lblLanguage);

		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setBounds(25, 99, 56, 16);
		contentPane.add(lblCategory);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Tamil", "English" }));
		comboBox.setBounds(126, 48, 117, 22);
		contentPane.add(comboBox);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] { "Movie", "Audio song", "Video song" }));
		comboBox_1.setBounds(126, 96, 117, 22);
		contentPane.add(comboBox_1);

		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(25, 172, 56, 16);
		contentPane.add(lblTitle);

		textField = new JTextField();
		textField.setBounds(127, 169, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(24, 218, 90, 16);
		contentPane.add(lblDescription);

		JTextPane textPane = new JTextPane();
		textPane.setBounds(126, 218, 254, 134);
		contentPane.add(textPane);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminOption.main(new String[] {});
			}
		});
		btnBack.setBounds(126, 400, 97, 25);
		contentPane.add(btnBack);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String Lang = comboBox.getSelectedItem().toString();
				String Cate = comboBox_1.getSelectedItem().toString();
				String Tit = textField.getText();
				String Des = textPane.getText();
				String Id = textField_1.getText();

				try {
					String sql = ("insert into library.add_cd (Language, Category, Title, Id, Description) values('"
							+ Lang + "','" + Cate + "','" + Tit + "','" + Id + "','" + Des + "')");
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Account created");
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnAdd.setBounds(281, 399, 97, 25);
		contentPane.add(btnAdd);

		JLabel lblId = new JLabel("Id:");
		lblId.setBounds(24, 138, 56, 16);
		contentPane.add(lblId);

		textField_1 = new JTextField();
		textField_1.setBounds(127, 132, 116, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
	}
}
