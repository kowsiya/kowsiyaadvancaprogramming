import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SignUp extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignUp frame = new SignUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection conn = null;
	private JLabel lblUsername;
	private JButton btnBack;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_7;
	private JComboBox comboBox_1;
	private JComboBox comboBox_2;
	private JPasswordField passwordField;

	public SignUp() throws SQLException {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/MyNewBookStore", "root", "root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// JOptionPane.showMessageDialog(null,"Success");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 505, 546);
		contentPane = new JPanel();
		contentPane.setToolTipText("Signup\r\n");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblName = new JLabel("Full name:");
		lblName.setBounds(67, 128, 94, 16);
		contentPane.add(lblName);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(67, 158, 94, 16);
		contentPane.add(lblPassword);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(185, 61, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(185, 94, 116, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JButton btnSignup = new JButton("Create");
		btnSignup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// String Id = textField.getText();
				String UName = textField_1.getText();
				String FName = textField_2.getText();
				String Pass = passwordField.getText();
				String Email = textField_4.getText();
				String PNumber = textField_5.getText();
				String Gender = comboBox_1.getSelectedItem().toString();
				String Ques = comboBox_2.getSelectedItem().toString();
				String Answer = textField_7.getText();

				try {
					String sql = ("insert into MyNewBookStore.signup (Username,Fullname,Password,Email,Phonenumber,Gender,Question,Answer) values('"
							+ UName + "','" + FName + "','" + Pass + "','" + Email + "','" + PNumber + "','" + Gender
							+ "','" + Ques + "','" + Answer + "')");
					java.sql.Statement st = conn.createStatement();
					st.executeUpdate(sql);
					JOptionPane.showMessageDialog(null, "Account created");
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnSignup.setBounds(287, 395, 97, 25);
		contentPane.add(btnSignup);

		JLabel lblId = new JLabel("Id:");
		lblId.setBounds(67, 65, 56, 16);
		contentPane.add(lblId);

		textField_2 = new JTextField();
		textField_2.setBounds(185, 125, 116, 22);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		lblUsername = new JLabel("User name:");
		lblUsername.setBounds(67, 95, 94, 16);
		contentPane.add(lblUsername);

		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login.main(new String[] {});
			}
		});
		btnBack.setBounds(120, 395, 97, 25);
		contentPane.add(btnBack);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(67, 193, 56, 16);
		contentPane.add(lblEmail);

		JLabel lblPhoneNumber = new JLabel("Phone number:");
		lblPhoneNumber.setBounds(67, 225, 107, 16);
		contentPane.add(lblPhoneNumber);

		JLabel lblGender = new JLabel("Gender:");
		lblGender.setBounds(67, 259, 56, 16);
		contentPane.add(lblGender);

		JLabel lblSecurityQuestion = new JLabel("Security question:");
		lblSecurityQuestion.setBounds(67, 292, 107, 16);
		contentPane.add(lblSecurityQuestion);

		JLabel lblAnswer = new JLabel("Answer:");
		lblAnswer.setBounds(67, 332, 56, 16);
		contentPane.add(lblAnswer);

		textField_4 = new JTextField();
		textField_4.setBounds(185, 190, 116, 22);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		textField_5 = new JTextField();
		textField_5.setBounds(185, 222, 116, 22);
		contentPane.add(textField_5);
		textField_5.setColumns(10);

		textField_7 = new JTextField();
		textField_7.setBounds(185, 329, 116, 22);
		contentPane.add(textField_7);
		textField_7.setColumns(10);

		comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] { "Male", "Female", "Other" }));
		comboBox_1.setBounds(186, 256, 115, 22);
		contentPane.add(comboBox_1);

		passwordField = new JPasswordField();
		passwordField.setBounds(185, 155, 116, 22);
		contentPane.add(passwordField);

		comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(
				new String[] { "What is your nickname?", "Your school?", "Your favourite game?" }));
		comboBox_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		comboBox_2.setBounds(185, 289, 168, 22);
		contentPane.add(comboBox_2);
	}
}
