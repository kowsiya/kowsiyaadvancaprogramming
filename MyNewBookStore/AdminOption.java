import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class AdminOption extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminOption frame = new AdminOption();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	Connection conn = null;

	public AdminOption() throws SQLException {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/MyNewBookStore", "root", "root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setTitle("Admin Options");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 387);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnShowUserdDetails = new JButton("User's details");
		btnShowUserdDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					View view = new View();
					view.show();
					String sql = ("select * from MyNewBookStore.signUp");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) view.table.getModel();
					tm.setRowCount(1);
				} catch (Exception e1) {
					System.out.println(e1);
				}
			}
		});
		btnShowUserdDetails.setBounds(58, 35, 152, 25);
		contentPane.add(btnShowUserdDetails);

		JButton btnShowBooksDetails = new JButton("Book's details");
		btnShowBooksDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					View view = new View();
					view.show();
					String sql = ("select * from MyNewBookStore.add_book");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) view.table.getModel();
					tm.setRowCount(1);

				} catch (Exception e2) {
					System.out.println(e2);
				}
			}
		});
		btnShowBooksDetails.setBounds(58, 83, 152, 25);
		contentPane.add(btnShowBooksDetails);

		JButton btnShowCdsDetails = new JButton("CD's details");
		btnShowCdsDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					View view = new View();
					view.show();
					String sql = ("select * from MyNewBookStore.add_cd");
					java.sql.Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) view.table.getModel();
					tm.setRowCount(1);

				} catch (Exception e3) {
					System.out.println(e3);
				}
			}
		});
		btnShowCdsDetails.setBounds(58, 130, 152, 25);
		contentPane.add(btnShowCdsDetails);

		JButton btnPaymentsDetails = new JButton("Payment's details");
		btnPaymentsDetails.setBounds(58, 179, 152, 25);
		contentPane.add(btnPaymentsDetails);

		JButton btnAddNewbook = new JButton("Add Newbook");
		btnAddNewbook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddBook.main(new String[] {});
			}
		});
		btnAddNewbook.setBounds(274, 83, 119, 25);
		contentPane.add(btnAddNewbook);

		JButton btnAddNewcd = new JButton("Add NewCD");
		btnAddNewcd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddCD.main(new String[] {});
			}
		});
		btnAddNewcd.setBounds(274, 130, 119, 25);
		contentPane.add(btnAddNewcd);

		JButton btnBack = new JButton("Back");
		btnBack.setBounds(58, 271, 97, 25);
		contentPane.add(btnBack);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnCancel.setBounds(183, 271, 97, 25);
		contentPane.add(btnCancel);
	}

}
