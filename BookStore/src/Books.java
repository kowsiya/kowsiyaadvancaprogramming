import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Books extends JFrame {

	public String title;
	public String author;
	public int isbn;
	public double price;
	public double discount;
	public double total;

public Books () {

}

	public void  Books(String title, String author, int isbn, double price,
			double discount, double total) {

		this.title = title;
		this.author = author;
		this.isbn = isbn;
		this.price = price;
		this.discount = discount;
		this.total = total;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public int getIsbn() {
		return isbn;
	}

	public double getPrice() {
		return price;
	}

	public double getDiscount() {
		return discount;
	}

	public double getTotal() {
		return total;
	}
}

	

